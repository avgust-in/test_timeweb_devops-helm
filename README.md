# Helm chart for deploy test_timeweb_devops

Application is used to deploy https://gitlab.com/avgust-in/test_timeweb_devops

tree:
```
.
├── helm
│   ├── Chart.yaml
│   ├── templates
│   │   ├── deploy.yml
│   │   ├── ingress-controller.yml
│   │   ├── ingress.yml
│   │   └── svc.yml
│   └── values.yaml
└── README.md
```


### Instruction

``` helm install your-app-name your-Chart.yml-directory```